# Design goals

- Support as many usecases as possible
  - But make most common usecases most ergonomic
- Be consistent with common conventions
  - Unless those conventions are crap

# Reference

## Pointer Events
[W3C Spec](https://w3c.github.io/pointerevents/)

[Androind docs: MotionEvent](https://developer.android.com/reference/android/view/MotionEvent)

[Androind docs: DragEvent](https://developer.android.com/reference/android/view/DragEvent)

## Keyboard Events
[W3C Spec](https://w3c.github.io/uievents/#events-keyboardevents)

[Android docs](https://developer.android.com/reference/android/view/KeyEvent)

## Wheel Events
[W3C Spec](https://w3c.github.io/uievents/#events-wheelevents)

## Gamepad

[W3C Spec](https://w3c.github.io/gamepad/#gamepad-interface) - not great

[Unity docs](https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0/manual/Gamepad.html)

[Unreal docs](https://docs.unrealengine.com/4.27/en-US/BlueprintAPI/Input/GamepadEvents/)

