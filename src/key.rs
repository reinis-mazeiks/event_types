use keyboard_types::{Code, Key, Location, Modifiers};
use typed_builder::TypedBuilder;

/// Describes a key's values, typically at the time of a key event
#[derive(TypedBuilder, Clone, PartialEq, Hash, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct KeyProperties {
    /// The effect of the key, commonly known as `key`
    ///
    /// Takes into consideration the state of modifier keys such as Shift as well as the keyboard locale and layout.
    effect: Key,
    /// The physical key being pressed, commonly known as `code`
    ///
    /// The value is not affected by the current keyboard layout or modifier state, so a particular key will always return the same value. This also means that this does not directly correspond to the character typed by the key.
    physical: Code,
    /// The set of modifier keys held
    modifiers: Modifiers,
    /// The location of the physical key on the keyboard or other input device
    location: Location,
    /// `true` iff the key is being held down so that it is automatically repeating
    is_auto_repeating: bool,
}

impl KeyProperties {
    /// The effect of the key, commonly known as `key`
    ///
    /// Takes into consideration the state of modifier keys such as Shift as well as the keyboard locale and layout.
    #[must_use]
    pub fn effect(&self) -> &Key {
        &self.effect
    }

    /// The physical key being pressed, commonly known as `code`
    ///
    /// The value is not affected by the current keyboard layout or modifier state, so a particular key will always return the same value. This also means that this does not directly correspond to the character typed by the key.
    #[must_use]
    pub fn physical(&self) -> Code {
        self.physical
    }

    /// The set of modifier keys held
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }

    /// The location of the physical key on the keyboard or other input device
    #[must_use]
    pub fn location(&self) -> Location {
        self.location
    }

    /// `true` iff the key is being held down so that it is automatically repeating
    #[must_use]
    pub fn is_auto_repeating(&self) -> bool {
        self.is_auto_repeating
    }
}

#[cfg(test)]
mod key_properties {
    use super::*;
    use assert2::assert;

    #[test]
    fn builder() {
        let k: KeyProperties = KeyProperties::builder()
            .effect(Key::Character("R".to_string()))
            .physical(Code::KeyR)
            .modifiers(Modifiers::SHIFT)
            .location(Location::default())
            .is_auto_repeating(false)
            .build();

        assert!(k.effect() == &Key::Character("R".to_string()));
        assert!(k.physical() == Code::KeyR);
        assert!(k.modifiers() == Modifiers::SHIFT);
        assert!(k.location() == Location::default());
        assert!(k.is_auto_repeating() == false);
    }
}
