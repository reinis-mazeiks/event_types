//! Collection of types describing data associated with some common input events
//!
//! Notably these do not have timestamps or references to the target element. Wrap these in your own Event<T> struct with such data.
//!
//! # Overview
//!
//! Pointer Events:
//! - Clicks: [`PrimaryClick`], [`AuxiliaryClick`], [`DoubleClick`]
//! - [`PointerEnter`]/[`PointerLeave`]: when the pointer enters or leaves an element
//!     - For devices that support hover (mouse, hover stylus), detects hovering on the element
//!     - On other devices (touch, stylus), triggered when the pointer is pressed down on the element
//! - [`PointerDown`]/[`PointerUp`]: when a pointer button is pressed/released, and no other button is held
//! - [`PointerMove`]: when some pointer property changes
//! - [`PointerCancel`]: when a pointer is unlikely to keep emitting events
//!
//! Keyboard Events:
//! - [`KeyDown`]: key pressed
//! - [`KeyUp`]: key released
//!
//! Wheel Events:
//! - [`WheelMove`]: wheel or similar device moved

use crate::pointer::{PointerButton, PointerProperties};
use crate::{KeyProperties, WheelDelta};
use keyboard_types::Modifiers;

/// Pointer button is pressed, when no other buttons are held
///
/// For mouse, this is when the device transitions from no buttons depressed to at least one button depressed.
///
/// For touch, this is when physical contact is made with the digitizer.
///
/// For pen, this is when the pen transitions from no buttons pressed + no contact to either contact or button pressed + hovering
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct PointerDown {
    pointer: PointerProperties,
    /// Modifier keys held at time of event
    modifiers: Modifiers,
    button_pressed: PointerButton,
    click_count: i64,
}

impl PointerDown {
    #[must_use]
    pub fn new(
        pointer: PointerProperties,
        modifiers: Modifiers,
        button_pressed: PointerButton,
        click_count: i64,
    ) -> Self {
        Self {
            pointer,
            modifiers,
            button_pressed,
            click_count,
        }
    }

    /// The properties of the pointer at the time of the event
    #[must_use]
    pub fn pointer(&self) -> &PointerProperties {
        &self.pointer
    }

    /// The button which triggered this event
    #[must_use]
    pub fn button_pressed(&self) -> &PointerButton {
        &self.button_pressed
    }

    /// The number of consecutive presses, including this one
    ///
    /// For example, if a mouse is only pressed down once, `click_count` will be 1.
    #[must_use]
    pub fn click_count(&self) -> i64 {
        self.click_count
    }

    /// Modifier keys held at time of event
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }
}

/// Last held pointer button is released
///
/// For mouse, this is when the device transitions from at least one button depressed to no buttons depressed.
///
/// For touch, this is when physical contact is removed.
///
/// For pen, this is when the pen transitions from either having contact or a pressed button while hovering to no buttons pressed + no contact
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct PointerUp {
    pointer: PointerProperties,
    /// Modifier keys held at time of event
    modifiers: Modifiers,
    button_released: PointerButton,
    click_count: i64,
}

impl PointerUp {
    #[must_use]
    pub fn new(
        pointer: PointerProperties,
        modifiers: Modifiers,
        button_released: PointerButton,
        click_count: i64,
    ) -> Self {
        Self {
            pointer,
            modifiers,
            button_released,
            click_count,
        }
    }

    /// The properties of the pointer at the time of the event
    #[must_use]
    pub fn pointer(&self) -> &PointerProperties {
        &self.pointer
    }

    /// Modifier keys held at time of event
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }

    /// The button which triggered this event
    #[must_use]
    pub fn button_released(&self) -> &PointerButton {
        &self.button_released
    }

    /// The number of consecutive presses, including this one
    ///
    /// For example, if a mouse is only pressed&released once, `click_count` will be 1.
    #[must_use]
    pub fn click_count(&self) -> i64 {
        self.click_count
    }
}

/// Pointer is moved into boundaries of an element
///
/// This may also be as a result of a pointer being pressed on a device that does not support hover
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct PointerEnter {
    pointer: PointerProperties,
    /// Modifier keys held at time of event
    modifiers: Modifiers,
}

impl PointerEnter {
    #[must_use]
    pub fn new(pointer: PointerProperties, modifiers: Modifiers) -> Self {
        Self { pointer, modifiers }
    }

    /// The properties of the pointer at the time of the event
    #[must_use]
    pub fn pointer(&self) -> &PointerProperties {
        &self.pointer
    }

    /// Modifier keys held at time of event
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }
}

/// Pointer leaves an element
///
/// This can happen when any of the following occurs:
///
/// - The pointing device is moved out of the hit test boundaries of an element.
/// - After a pointer is released for a device that does not support hover.
/// - The user agent has detected a scenario to suppress a pointer event stream.
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct PointerLeave {
    pointer: PointerProperties,
    /// Modifier keys held at time of event
    modifiers: Modifiers,
}

impl PointerLeave {
    #[must_use]
    pub fn new(pointer: PointerProperties, modifiers: Modifiers) -> Self {
        Self { pointer, modifiers }
    }

    /// The properties of the pointer at the time of the event
    #[must_use]
    pub fn pointer(&self) -> &PointerProperties {
        &self.pointer
    }

    /// Modifier keys held at time of event
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }
}

/// Some pointer property changes
///
/// This includes any changes to coordinates, pressure, tangential pressure, tilt, twist, dimensions or pressing/releasing buttons
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct PointerMove {
    pointer: PointerProperties,
    /// Modifier keys held at time of event
    modifiers: Modifiers,
}

impl PointerMove {
    #[must_use]
    pub fn new(pointer: PointerProperties, modifiers: Modifiers) -> Self {
        Self { pointer, modifiers }
    }

    /// The properties of the pointer at the time of the event
    #[must_use]
    pub fn pointer(&self) -> &PointerProperties {
        &self.pointer
    }
    /// Modifier keys held at time of event
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }
}

/// Fires upon detecting a pointer is unlikely to continue to produce events.
///
/// Any of the following scenarios satisfy this condition (there MAY be additional scenarios):
///
/// - The user agent has opened a modal dialog or menu.
/// - A pointer input device is physically disconnected, or a hoverable pointer input device (e.g. a hoverable pen/stylus) has left the hover range detectable by the digitizer.
/// - The pointer is subsequently used by the user agent to manipulate the page viewport (e.g. panning or zooming). See the section on touch-action CSS property for details.
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct PointerCancel {
    pointer: PointerProperties,
    /// Modifier keys held at time of event
    modifiers: Modifiers,
}

impl PointerCancel {
    #[must_use]
    pub fn new(pointer: PointerProperties, modifiers: Modifiers) -> Self {
        Self { pointer, modifiers }
    }

    /// The properties of the pointer at the time of the event
    #[must_use]
    pub fn pointer(&self) -> &PointerProperties {
        &self.pointer
    }
    /// Modifier keys held at time of event
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }
}

/// Pointer clicks on an element with the primary button
///
/// For a mouse/stylus, this means pressing and releasing the primary button
///
/// For touch, this means touching/releasing
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct PrimaryClick {
    pointer: PointerProperties,
    /// Modifier keys held at time of event
    modifiers: Modifiers,
    click_count: i64,
}

impl PrimaryClick {
    #[must_use]
    pub fn new(pointer: PointerProperties, modifiers: Modifiers, click_count: i64) -> Self {
        Self {
            pointer,
            modifiers,
            click_count,
        }
    }

    /// The properties of the pointer at the time of the event
    #[must_use]
    pub fn pointer(&self) -> &PointerProperties {
        &self.pointer
    }

    /// The number of consecutive presses, including this one
    ///
    /// For example, if a mouse is only clicked once, `click_count` will be 1.
    #[must_use]
    pub fn click_count(&self) -> i64 {
        self.click_count
    }

    /// Modifier keys held at time of event
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }
}

/// Pointer clicks twice on an element with the primary button
///
/// For a mouse, this means pressing and releasing the primary button twice
///
/// For touch, this means touching/releasing twice
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct DoubleClick {
    pointer: PointerProperties,
    /// Modifier keys held at time of event
    modifiers: Modifiers,
    click_count: i64,
}

impl DoubleClick {
    #[must_use]
    pub fn new(pointer: PointerProperties, modifiers: Modifiers, click_count: i64) -> Self {
        Self {
            pointer,
            modifiers,
            click_count,
        }
    }

    /// The properties of the pointer at the time of the event
    #[must_use]
    pub fn pointer(&self) -> &PointerProperties {
        &self.pointer
    }

    /// The number of consecutive presses, including this one
    ///
    /// For example, if a mouse is double-clicked, `click_count` will be 2.
    #[must_use]
    pub fn click_count(&self) -> i64 {
        self.click_count
    }

    /// Modifier keys held at time of event
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }
}

/// Pointer clicks on an element with something other than the primary button
///
/// For a mouse/stylus, this means pressing and releasing the button
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct AuxiliaryClick {
    pointer: PointerProperties,
    /// Modifier keys held at time of event
    modifiers: Modifiers,
    clicked_button: PointerButton,
    click_count: i64,
}

impl AuxiliaryClick {
    #[must_use]
    pub fn new(
        pointer: PointerProperties,
        modifiers: Modifiers,
        clicked_button: PointerButton,
        click_count: i64,
    ) -> Self {
        Self {
            pointer,
            modifiers,
            clicked_button,
            click_count,
        }
    }

    /// The properties of the pointer at the time of the event
    #[must_use]
    pub fn pointer(&self) -> &PointerProperties {
        &self.pointer
    }

    /// The button which triggered this event
    #[must_use]
    pub fn clicked_button(&self) -> &PointerButton {
        &self.clicked_button
    }

    /// The number of consecutive presses, including this one
    ///
    /// For example, if a mouse is only clicked once, `click_count` will be 1.
    #[must_use]
    pub fn click_count(&self) -> i64 {
        self.click_count
    }

    /// Modifier keys held at time of event
    #[must_use]
    pub fn modifiers(&self) -> Modifiers {
        self.modifiers
    }
}

/// A key is pressed down
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct KeyDown {
    key: KeyProperties,
}

impl KeyDown {
    #[must_use]
    pub fn new(key: KeyProperties) -> Self {
        Self { key }
    }

    /// Properties of key at time of event
    #[must_use]
    pub fn key(&self) -> &KeyProperties {
        &self.key
    }
}

/// A key is released
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct KeyUp {
    key: KeyProperties,
}

impl KeyUp {
    #[must_use]
    pub fn new(key: KeyProperties) -> Self {
        Self { key }
    }

    /// Properties of key at time of event
    #[must_use]
    pub fn key(&self) -> &KeyProperties {
        &self.key
    }
}

/// Mouse wheel has been rotated around any axis
///
/// Also triggered when an equivalent input device (such as a mouse-ball, certain tablets or touchpads, etc.) has emulated such an action
#[derive(Clone, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct WheelMove {
    delta: WheelDelta,
}

impl WheelMove {
    #[must_use]
    pub fn new(delta: WheelDelta) -> Self {
        Self { delta }
    }

    /// Amount that the page would normally scroll due to this event
    #[must_use]
    pub fn delta(&self) -> WheelDelta {
        self.delta
    }
}
