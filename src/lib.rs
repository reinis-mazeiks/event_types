//! Types to help idiomatically represent user input events, such as pointer clicks and moves.
//!
//! ```
//! use euclid::num::Zero;
//! use keyboard_types::Modifiers;
//! use event_types::event_data::PointerDown;
//! use event_types::*;
//!
//! // Your library can construct event data
//! let pointer_data = PointerProperties::builder()
//!             .coordinates(Coordinates::zero())
//!             .held_buttons(PointerButton::Primary)
//!             .id(PointerId(0))
//!             .pointer_type(PointerType::Mouse)
//!             .is_primary(true)
//!             .build();
//! let event_data = PointerDown::new(pointer_data, Modifiers::SHIFT, PointerButton::Primary, 1);
//!
//! // So that your users can get info about input events
//! dbg!(event_data.button_pressed());
//! dbg!(event_data.pointer());
//! dbg!(event_data.modifiers());
//! ```
//!
//! # Overview
//!
//! - The [`event_data`] module: Collection of types describing some common input events
//! - [`KeyProperties`]: Describes a key
//! - [`PointerProperties`]: Describes a pointer
//!
//! The rest are mostly small structs and enums helping describe event data and its units.
//!
//! # Usage
//!
//! This library only provides types to represent data associated with input events. It does not provide an event system itself. Therefore, if you are writing a GUI framework, you will most likely have to bring:
//! - Your own Event wrapper, which will contain:
//!     - Event data from structs in [`event_data`] (possibly in a Vec if you want to batch some events)
//!     - Some sort of reference to the target element
//!     - Facilities for default event behavior and cancelling it, if desired
//!     - Event bubbling and preventing it, if desired
//! - Your own event data for events not in this crate (e.g. clipboard events)
//! - Your own components and ways to attach event listeners to them

#![warn(clippy::all, clippy::pedantic, clippy::cargo)]
// `derive(EnumSetType)` creates its own PartialEq; should be fine to derive Hash.
#![allow(clippy::derive_hash_xor_eq)]
// too pedantic
#![allow(clippy::module_name_repetitions)]

/// A re-export of [enumset]
///
/// Used for describing sets of pointer buttons
pub use enumset;
/// A re-export of [euclid]
///
/// Used for geometry primitives, including coordinates and dimensions
pub use euclid;
/// A re-export of [`keyboard_types`]
///
/// Used for describing keys and active modifiers in pointer events
pub use keyboard_types;

pub use geometry::*;
pub use key::*;
pub use pointer::*;

pub mod coordinate_space;
pub mod event_data;
mod geometry;
mod key;
mod pointer;
