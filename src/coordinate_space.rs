//! Coordinate space marker structs
//!
//! In [euclid], the coordinate and unit types are specified with a marker struct. This module contains units used to describe event data.

/// Coordinate space relative to the screen
///
/// The screen's top-left corner is (0, 0). X coordinates increase to the right, and Y coordinates increase downward.
///
/// This takes into account the window offset. For example, if the window is moved 200 units right and 300 units down, screen-space coordinates will be offset by (200, 300)
pub struct ScreenSpace;

/// Coordinate space relative to the page
///
/// The page's top-left corner is (0, 0). X coordinates increase to the right, and Y coordinates increase downward.
///
/// This means that, if the page has been scrolled 200 units right and 300 units down, the top-left corner of the viewport corresponds to page coordinates (200, 300).
pub struct PageSpace;

/// Coordinate space relative to the viewport
///
/// The viewport's top-left corner is (0, 0). X coordinates increase to the right, and Y coordinates increase downward.
///
/// This means that the top-left corner of the viewport always corresponds to (0, 0), regardless of how much the page has been scrolled.
pub struct ClientSpace;

/// Coordinate space relative to an element
///
/// The element's top-left corner is (0, 0). X coordinates increase to the right, and Y coordinates increase downward.
pub struct ElementSpace;

/// A pixel unit: one unit corresponds to 1 pixel
pub struct Pixels;

/// A unit in terms of Lines
///
/// One unit is relative to the size of one line
pub struct Lines;

/// A unit in terms of Screens:
///
/// One unit is relative to the size of a page
pub struct Pages;
