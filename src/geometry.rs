use crate::coordinate_space::{
    ClientSpace, ElementSpace, Lines, PageSpace, Pages, Pixels, ScreenSpace,
};
use euclid::num::Zero;
use euclid::{Angle, Point2D, UnknownUnit, Vector3D};

/// A point in [`ScreenSpace`]
pub type ScreenPoint = Point2D<f64, ScreenSpace>;

/// A point in [`PageSpace`]
pub type PagePoint = Point2D<f64, PageSpace>;

/// A point in [`ClientSpace`]
pub type ClientPoint = Point2D<f64, ClientSpace>;

/// A point in [`ElementSpace`]
pub type ElementPoint = Point2D<f64, ElementSpace>;

/// A vector expressed in [Pixels]
pub type PixelsVector = Vector3D<f64, Pixels>;

/// A vector expressed in [Lines]
pub type LinesVector = Vector3D<f64, Lines>;

/// A vector expressed in [Pages]
pub type PagesVector = Vector3D<f64, Pages>;

/// A vector representing the movement of the mouse wheel
///
/// This may be expressed in [Pixels], [Lines] or [Pages]
#[derive(Copy, Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum WheelDelta {
    /// Movement in Pixels
    Pixels(PixelsVector),
    /// Movement in Lines
    Lines(LinesVector),
    /// Movement in Pages
    Pages(PagesVector),
}

impl Zero for WheelDelta {
    fn zero() -> Self {
        WheelDelta::pixels(0., 0., 0.)
    }
}

impl WheelDelta {
    /// Convenience function for constructing a `WheelDelta` with pixel units
    #[must_use]
    pub fn pixels(x: f64, y: f64, z: f64) -> Self {
        WheelDelta::Pixels(PixelsVector::new(x, y, z))
    }

    /// Convenience function for constructing a `WheelDelta` with line units
    #[must_use]
    pub fn lines(x: f64, y: f64, z: f64) -> Self {
        WheelDelta::Lines(LinesVector::new(x, y, z))
    }

    /// Convenience function for constructing a `WheelDelta` with page units
    #[must_use]
    pub fn pages(x: f64, y: f64, z: f64) -> Self {
        WheelDelta::Pages(PagesVector::new(x, y, z))
    }

    /// Returns true iff there is no wheel movement
    ///
    /// i.e. the x, y and z delta is zero (disregarding units)
    #[must_use]
    pub fn is_zero(&self) -> bool {
        self.strip_units() == Vector3D::zero()
    }

    /// A `Vector3D` proportional to the amount scrolled
    ///
    /// Note that this disregards the 3 possible units: this could be expressed in terms of pixels, lines, or pages.
    ///
    /// In most cases, to properly handle scrolling, you should handle all 3 possible enum variants instead of stripping units. Otherwise, if you assume that the units will always be pixels, the user may experience some unexpectedly slow scrolling if their mouse/OS sends values expressed in lines or pages.
    #[must_use]
    pub fn strip_units(&self) -> Vector3D<f64, UnknownUnit> {
        match self {
            WheelDelta::Pixels(v) => v.cast_unit(),
            WheelDelta::Lines(v) => v.cast_unit(),
            WheelDelta::Pages(v) => v.cast_unit(),
        }
    }
}

#[cfg(test)]
mod wheel_delta {
    use super::*;

    #[test]
    fn zero() {
        let d = WheelDelta::zero();
        assert_eq!(d, WheelDelta::Pixels(PixelsVector::new(0., 0., 0.,)));
        assert!(d.is_zero());
    }

    #[test]
    fn construct_pixels() {
        let d = WheelDelta::pixels(1., 2., 3.);
        assert_eq!(d, WheelDelta::Pixels(PixelsVector::new(1., 2., 3.,)));
    }

    #[test]
    fn construct_lines() {
        let d = WheelDelta::lines(1., 2., 3.);
        assert_eq!(d, WheelDelta::Lines(LinesVector::new(1., 2., 3.,)));
    }

    #[test]
    fn construct_pages() {
        let d = WheelDelta::pages(1., 2., 3.);
        assert_eq!(d, WheelDelta::Pages(PagesVector::new(1., 2., 3.,)));
    }

    #[test]
    fn strip_units() {
        let d = WheelDelta::pixels(1., 2., 3.);
        assert_eq!(d.strip_units(), Vector3D::new(1., 2., 3.));
    }
}

/// Coordinates of a point in a user interface
#[derive(Debug, Clone, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Coordinates {
    screen: ScreenPoint,
    page: PagePoint,
    client: ClientPoint,
    element: ElementPoint,
}

impl Coordinates {
    /// Construct new coordinates with the specified screen-, client-, element- and page-relative points
    #[must_use]
    pub fn new(
        screen: ScreenPoint,
        page: PagePoint,
        client: ClientPoint,
        element: ElementPoint,
    ) -> Self {
        Self {
            screen,
            page,
            client,
            element,
        }
    }
    /// Coordinates relative to the entire screen. This takes into account the window's offset.
    #[must_use]
    pub fn screen(&self) -> ScreenPoint {
        self.screen
    }

    /// Coordinates relative to the entire document. This includes any portion of the document not currently visible.
    ///
    /// For example, if the page is scrolled 200 pixels to the right and 300 pixels down, the top left corner of the viewport corresponds to page coordinates (200., 300.)
    #[must_use]
    pub fn page(&self) -> PagePoint {
        self.page
    }

    /// Coordinates relative to the application's viewport (as opposed to the coordinates within the page).
    ///
    /// For example, the top left corner of the viewport corresponds to client coordinates (0., 0.), regardless of whether the page is scrolled.
    #[must_use]
    pub fn client(&self) -> ClientPoint {
        self.client
    }

    /// Coordinates relative to the top-left corner of the target element
    ///
    /// For example, the top left corner of an element corresponds to element coordinates (0., 0.)
    #[must_use]
    pub fn element(&self) -> ElementPoint {
        self.element
    }
}

impl Zero for Coordinates {
    fn zero() -> Self {
        Self::new(
            ScreenPoint::zero(),
            PagePoint::zero(),
            ClientPoint::zero(),
            ElementPoint::zero(),
        )
    }
}

#[cfg(test)]
mod coordinates {
    use super::*;

    #[test]
    fn getters() {
        let c = Coordinates::new(
            ScreenPoint::new(1., 1.),
            PagePoint::new(2., 2.),
            ClientPoint::new(3., 3.),
            ElementPoint::new(4., 4.),
        );

        assert_eq!(c.screen(), ScreenPoint::new(1., 1.));
        assert_eq!(c.page(), PagePoint::new(2., 2.));
        assert_eq!(c.client(), ClientPoint::new(3., 3.));
        assert_eq!(c.element(), ElementPoint::new(4., 4.));
    }
}

/// The orientation (tilt or spherical angles, and twist) of a transducer (e.g. pen/stylus)
#[derive(PartialEq, Debug, Clone)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct PointerOrientation {
    tilt_x: Angle<f64>,
    tilt_y: Angle<f64>,

    altitude: Angle<f64>,
    azimuth: Angle<f64>,

    twist: Angle<f64>,
}

impl PointerOrientation {
    /// Construct a new `PointerAngle` by specifying its tilt, spherical angle and twist
    #[must_use]
    pub fn new(
        tilt_x: Angle<f64>,
        tilt_y: Angle<f64>,
        altitude: Angle<f64>,
        azimuth: Angle<f64>,
        twist: Angle<f64>,
    ) -> Self {
        Self {
            tilt_x,
            tilt_y,
            altitude,
            azimuth,
            twist,
        }
    }

    /// Construct a new `PointerAngle` by specifying the tilt angles and twist
    ///
    /// Spherical angles (altitude and azimuth) will be calculated automatically
    #[must_use]
    pub fn from_tilt_and_twist(tilt_x: Angle<f64>, tilt_y: Angle<f64>, twist: Angle<f64>) -> Self {
        let (altitude, azimuth) = tilt_to_spherical(tilt_x, tilt_y);

        Self {
            tilt_x,
            tilt_y,
            altitude,
            azimuth,
            twist,
        }
    }

    /// Construct a new `PointerAngle` by specifying the spherical angles and twist
    ///
    /// Tilt angles (`tilt_x` and `tilt_y`) will be calculated automatically
    #[must_use]
    pub fn from_spherical_and_twist(
        altitude: Angle<f64>,
        azimuth: Angle<f64>,
        twist: Angle<f64>,
    ) -> Self {
        let (tilt_x, tilt_y) = spherical_to_tilt(altitude, azimuth);

        Self {
            tilt_x,
            tilt_y,
            altitude,
            azimuth,
            twist,
        }
    }

    /// The plane angle (in the range of \[-PI/2,PI/2\]) between the Y-Z plane and the plane containing both a transducer (e.g. pen/stylus) axis and the Y axis.
    ///
    /// A positive value is to the right, in the direction of increasing X values. For hardware and platforms that do not report tilt, the value must be zero.
    #[must_use]
    pub fn tilt_x(&self) -> Angle<f64> {
        self.tilt_x
    }

    /// The plane angle in the range of \[-PI/2,PI/2\]) between the X-Z plane and the plane containing both a transducer (e.g. pen/stylus) axis and the X axis.
    ///
    /// A positive value is towards the user, in the direction of increasing Y values. For hardware and platforms that do not report tilt, the value must be zero.
    #[must_use]
    pub fn tilt_y(&self) -> Angle<f64> {
        self.tilt_y
    }

    /// The altitude of a transducer (e.g. pen/stylus), in the range \[0,π/2\]
    ///
    /// 0 is parallel to the surface (X-Y plane), and π/2 is perpendicular to the surface. For hardware and platforms that do not report tilt or angle, the value must be π/2.
    #[must_use]
    pub fn altitude(&self) -> Angle<f64> {
        self.altitude
    }

    /// The azimuth angle of a transducer (e.g. pen/stylus), in the range [0, 2π]
    ///
    /// 0 represents a transducer whose cap is pointing in the direction of increasing X values (point to "3 o'clock" if looking straight down) on the X-Y plane, and the values progressively increase when going clockwise (π/2 at "6 o'clock", π at "9 o'clock", 3π/2 at "12 o'clock").
    ///
    /// When a transducer is perfectly perpendicular to the surface (altitudeAngle of π/2), the value must be 0. For hardware and platforms that do not report tilt or angle, the value MUST be 0.
    #[must_use]
    pub fn azimuth(&self) -> Angle<f64> {
        self.azimuth
    }

    /// The clockwise rotation of a transducer (e.g. pen/stylus) around its own major axis, in the range \[0-2π\]
    ///
    /// For hardware and platforms that do not report twist, the value MUST be 0.
    #[must_use]
    pub fn twist(&self) -> Angle<f64> {
        self.twist
    }
}

impl Default for PointerOrientation {
    /// Default orientation: perpendicular to the surface
    ///
    /// All angles are zero except for altitude, which is π/2
    fn default() -> Self {
        Self::new(
            Angle::zero(),
            Angle::zero(),
            Angle::frac_pi_2(),
            Angle::zero(),
            Angle::zero(),
        )
    }
}

#[cfg(test)]
mod pointer_orientation {
    use super::*;
    use assert2::assert;
    use euclid::approxeq::ApproxEq;

    #[test]
    fn default() {
        let p = PointerOrientation::default();

        assert!(p.azimuth() == Angle::zero());
        assert!(p.altitude() == Angle::frac_pi_2());
        assert!(p.tilt_x() == Angle::zero());
        assert!(p.tilt_y() == Angle::zero());
        assert!(p.twist() == Angle::zero());
    }

    #[test]
    fn twist() {
        let p = PointerOrientation::from_tilt_and_twist(
            Angle::zero(),
            Angle::zero(),
            Angle::degrees(42.),
        );
        assert!(p.twist() == Angle::degrees(42.));

        let p = PointerOrientation::from_spherical_and_twist(
            Angle::zero(),
            Angle::zero(),
            Angle::degrees(42.),
        );
        assert!(p.twist() == Angle::degrees(42.));
    }

    #[test]
    fn from_tilt() {
        let p =
            PointerOrientation::from_tilt_and_twist(Angle::zero(), Angle::zero(), Angle::default());
        assert!(p.azimuth() == Angle::zero());
        assert!(p.altitude() == Angle::frac_pi_2());
        assert!(p.tilt_x() == Angle::zero());
        assert!(p.tilt_y() == Angle::zero());

        let p = PointerOrientation::from_tilt_and_twist(
            Angle::frac_pi_4(),
            Angle::zero(),
            Angle::default(),
        );
        assert!(p.azimuth() == Angle::zero());
        assert!(p.altitude() == Angle::frac_pi_4());
        assert!(p.tilt_x() == Angle::frac_pi_4());
        assert!(p.tilt_y() == Angle::zero());

        let p = PointerOrientation::from_tilt_and_twist(
            Angle::zero(),
            Angle::frac_pi_4(),
            Angle::default(),
        );
        assert!(p.azimuth() == Angle::frac_pi_2());
        assert!(p.altitude() == Angle::frac_pi_4());
        assert!(p.tilt_x() == Angle::zero());
        assert!(p.tilt_y() == Angle::frac_pi_4());

        let p = PointerOrientation::from_tilt_and_twist(
            -Angle::frac_pi_4(),
            Angle::zero(),
            Angle::default(),
        );
        assert!(p.azimuth() == Angle::pi());
        assert!(p.altitude() == Angle::frac_pi_4());
        assert!(p.tilt_x() == -Angle::frac_pi_4());
        assert!(p.tilt_y() == Angle::zero());

        // Example produced with GeoGebra calculator
        // https://www.geogebra.org/calculator/ejgtzkyd
        let p = PointerOrientation::from_tilt_and_twist(
            Angle::degrees(70.),
            Angle::degrees(30.),
            Angle::default(),
        );
        assert!(p.altitude().to_degrees().approx_eq_eps(&19.61, &0.1));
        // crazy loss of precision idk
        assert!(p.azimuth().to_degrees().approx_eq_eps(&11.17, &1.));
    }

    #[test]
    fn from_spherical() {
        let p = PointerOrientation::from_spherical_and_twist(
            Angle::frac_pi_4(),
            Angle::zero(),
            Angle::default(),
        );
        assert!(p.tilt_x().approx_eq(&Angle::frac_pi_4()));
        assert!(p.tilt_y().approx_eq(&Angle::zero()));

        let p = PointerOrientation::from_spherical_and_twist(
            Angle::frac_pi_4(),
            Angle::frac_pi_2(),
            Angle::default(),
        );
        assert!(p.tilt_x().approx_eq(&Angle::zero()));
        assert!(p.tilt_y().approx_eq(&Angle::frac_pi_4()));

        let p = PointerOrientation::from_spherical_and_twist(
            Angle::frac_pi_4(),
            Angle::pi(),
            Angle::default(),
        );
        assert!(p.tilt_x().approx_eq(&-Angle::frac_pi_4()));
        assert!(p.tilt_y().approx_eq(&Angle::zero()));

        // Example produced with GeoGebra calculator
        // https://www.geogebra.org/calculator/ejgtzkyd
        let p = PointerOrientation::from_spherical_and_twist(
            Angle::degrees(19.61),
            Angle::degrees(11.17),
            Angle::default(),
        );
        assert!(p.tilt_x().to_degrees().approx_eq_eps(&70., &0.1));
        // crazy loss of precision?
        assert!(p.tilt_y().to_degrees().approx_eq_eps(&30., &2.));
    }
}

/// Convert spherical angles (altitude + azimuth) to tilt (tilt X & tilt Y)
///
/// According to [web spec](https://w3c.github.io/pointerevents/#converting-between-tiltx-tilty-and-altitudeangle-azimuthangle)
fn spherical_to_tilt(altitude: Angle<f64>, azimuth: Angle<f64>) -> (Angle<f64>, Angle<f64>) {
    use std::f64::consts::{FRAC_PI_2 as HALF_PI, PI};
    const THREE_HALF_PI: f64 = PI / 2. * 3.;
    const TWO_PI: f64 = 2. * PI;

    if altitude == Angle::zero() {
        let a = azimuth.radians;
        let (tilt_x, tilt_y) = if a == 0. || a == TWO_PI {
            (HALF_PI, 0.)
        } else if 0. < a && a < HALF_PI {
            (HALF_PI, HALF_PI)
        } else if a == HALF_PI {
            (0., HALF_PI)
        } else if HALF_PI < a && a < PI {
            (-HALF_PI, HALF_PI)
        } else if a == PI {
            (-HALF_PI, 0.)
        } else if PI < a && a < THREE_HALF_PI {
            (-HALF_PI, -HALF_PI)
        } else if a == THREE_HALF_PI {
            (0., -HALF_PI)
        } else if THREE_HALF_PI < a && a < TWO_PI {
            (HALF_PI, -HALF_PI)
        } else {
            panic!("invalid value for azimuth {azimuth:?}")
        };

        (Angle::radians(tilt_x), Angle::radians(tilt_y))
    } else {
        let altitude_tan = altitude.radians.tan();
        let (azimuth_sin, azimuth_cos) = azimuth.sin_cos();

        let tilt_x = (azimuth_cos / altitude_tan).atan();
        let tilt_y = (azimuth_sin / altitude_tan).atan();

        (Angle::radians(tilt_x), Angle::radians(tilt_y))
    }
}

/// Convert tilt angles (tilt X & tilt Y) to spherical (altitude + azimuth)
///
/// According to [web spec](https://w3c.github.io/pointerevents/#converting-between-tiltx-tilty-and-altitudeangle-azimuthangle)
fn tilt_to_spherical(tilt_x: Angle<f64>, tilt_y: Angle<f64>) -> (Angle<f64>, Angle<f64>) {
    use std::f64::consts::{FRAC_PI_2 as HALF_PI, PI};
    const THREE_HALF_PI: f64 = PI / 2. * 3.;
    const TWO_PI: f64 = 2. * PI;

    let x = tilt_x.radians;
    let y = tilt_y.radians;

    let azimuth = if x == 0. {
        if y < 0. {
            THREE_HALF_PI
        } else if y > 0. {
            HALF_PI
        } else {
            0.
        }
    } else if y == 0. {
        if x < 0. {
            PI
        } else {
            0.
        }
    } else if x.abs() == HALF_PI || y.abs() == HALF_PI {
        // not enough information to calculate azimuth
        0.
    } else {
        let tan_x = x.tan();
        let tan_y = y.tan();

        let a = tan_y.atan2(tan_x);

        if a < 0. {
            a + TWO_PI
        } else {
            a
        }
    };

    let altitude = if x.abs() == HALF_PI || y.abs() == HALF_PI {
        0.
    } else if x == 0. {
        HALF_PI - y.abs()
    } else if y == 0. {
        HALF_PI - x.abs()
    } else {
        let x_tan_square = x.tan().powi(2);
        let y_tan_square = y.tan().powi(2);

        (1. / (x_tan_square + y_tan_square).sqrt()).atan()
    };

    (Angle::radians(altitude), Angle::radians(azimuth))
}
