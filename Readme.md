# Rust Input Event Types

A library with some types to help idiomatically represent user input events, such as pointer events, wheel events, and keyboard events.

Some conventions are taken from the W3C web specifications, but the data types are re-structured for a nicer Rust API.

## Non-features

The crate knows nothing about event handlers, default behaviors, bubbling, event batching, windows, the web, the DOM, or predictive events. Those are all nice things, but we want this crate to be usable in any GUI framework – so we can't make any assumptions about how you want to handle events, or how you represent your user interface. We merely offer a set of types to build your own event system with.

Does not include all web events. For example, we have no mouse events, since pointer events are more general in that they can work with any type of pointer, including the mouse. This is better in a world where software needs to support a wide range of devices.

## Dependencies

- [`euclid`](https://crates.io/crates/euclid) for geometry primitives (such as pointer locations)
- [`keyboard-types`](https://crates.io/crates/keyboard-types) for keys & modifiers
- [`enumset`](https://crates.io/crates/enumset) for bit sets of pointer buttons
- [`typed-builder`](https://crates.io/crates/typed-builder) as an ergonomic API for large struct construction
